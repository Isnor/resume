# resume.isnor.ca

The source code for my portfolio site [resume.isnor.ca](https://resume.isnor.ca), created using [Hugo](https://gohugo.io).

## Overview

[resume.isnor.ca](https://resume.isnor.ca) is a simple web portfolio of my professional experience. The purpose of this project is to try to have some fun and to replace my horrible old personal website

## Contents

The layout of the repository is:
- `hugo/` - the source code for the portfolio website
- `terraform/` - the infrastructure-as-code to provision the cloud resources used to host the website
- `vale/` - [`vale`](https://github.com/errata-ai/vale) styles
- `.gitlab-ci.yml` - the GitLab CI/CD configuration
- `.gitlab/ci/` - CI/CD pipeline configurations


## Updating the resume

Use the `staging` branch and push to it directly, it will deploy to the gitlab Pages site. To update the resume, see the files in `hugo/data/en/sections/`, e.g. `experience.yaml` is where the job experiences are written. Create a merge request from staging->main to "deploy to production", which will update the actual site.

If you're going to diddle with the theme, then you need to `git submodule init` and update it first.

You can run `vale hugo` locally to run the static analysis before pushing it up.

## Resources and Thanks

- [GitLab](https://gitlab.com), the best way to host and manage projects with `git`
- [Hugo](https://gohugo.io), the static site generator used to build this site
  - [Toha](https://toha-guides.netlify.app/posts/), the theme used for the site
- [SVGRepo](https://www.svgrepo.com/), a fantastic repository of SVGs without which the portfolio wouldn't look as nice
- [BGJar](https://bgjar.com), an awesome site for making various backgrounds using SVG
- [ConvertingColors](https://convertingcolors.com), a site for helping with colours
- [Formito's Favicon Generator](https://formito.com/tools/favicon), a tool for creating simple SVG favicons
- [Vale](https://github.com/errata-ai/vale), static analysis and doc checking / reviewing