
data "aws_iam_policy_document" "resume_bucket_access_policy" {

  # allow our resume cloudfront OAI to read from the bucket
  statement {
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${local.resume_bucket_name}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.resume_cloudfront.iam_arn]
    }
  }
}

resource "aws_s3_bucket" "website_bucket" {
  bucket = local.resume_bucket_name
}

resource "aws_s3_bucket_policy" "website_bucket" {
  bucket = aws_s3_bucket.website_bucket.id
  policy = data.aws_iam_policy_document.resume_bucket_access_policy.json
}

resource "aws_s3_bucket_website_configuration" "website_configuration" {
  bucket = aws_s3_bucket.website_bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "404.html"
  }

}